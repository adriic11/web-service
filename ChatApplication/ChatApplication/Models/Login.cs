﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatApplication.Models
{
    public class Login
    {
        public string Nombre{ get; set; }
        public string Password { get; set; }
        public string Canal { get; set; }
        public string Mensaje { get; set; }
        public Nullable<System.TimeSpan> Fecha { get; set; }
    }
}