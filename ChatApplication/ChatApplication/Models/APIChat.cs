using ChatApplication.Datos;
using System;
using System.Collections.Generic;

namespace ChatApplication.Models
{ 
    public partial class APIChat
    {
        public int ChatID { get; set; }
        public string Nombre { get; set; }
        public string Password { get; set; }
        public string Canal { get; set; }
        public string Mensaje { get; set; }
        public Nullable<System.TimeSpan> Fecha { get; set; }

        public static Respuesta AgregarUsuario(APIChat chat)
        {
            List<Parametro> parametros = new List<Parametro>
             {
                 new Parametro("@Nombre", chat.Nombre),
                 new Parametro("@Password", chat.Password)
             };

            return DBDatos.Ejecutar("Usuario_Agregar", parametros);
        }

        public static Respuesta AgregarCanal(APIChat chat)
        {
            List<Parametro> parametros = new List<Parametro>
             {
                 new Parametro("@Nombre", chat.Nombre),
                 new Parametro("@Password", chat.Password),
                 new Parametro("@Canal",chat.Canal)
             };

            return DBDatos.Ejecutar("Canal_Agregar", parametros);
        }
    }

    
}
