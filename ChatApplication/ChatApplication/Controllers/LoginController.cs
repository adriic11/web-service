﻿using ChatApplication.Datos;
using ChatApplication.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace ChatApplication.Controllers
{
    public class LoginController : ApiController
    {
        Respuesta respuesta = new Respuesta();

        [Route("api/usuario/Crear")]
        [HttpPost]
        public dynamic Crear(APIChat usuario)
        {
            return APIChat.AgregarUsuario(usuario);
            
        }

        [Route("api/usuario/Canal")]
        [HttpPost]
        public dynamic AgregarCanal(APIChat canal)
        {
            APIChat.AgregarCanal(canal);
            respuesta.mensaje = canal.Canal; 
            return Ok("Te has unido al canal " + respuesta.mensaje);
        }

        [Route("api/usuario/Mensaje")]
        [HttpGet]
        public dynamic Add([FromBody] Login login)
        {
            respuesta.resultado = 0;
            DataTable table = new DataTable();

            TimeSpan tiempo = DateTime.Now.TimeOfDay;
            try
            {
                using (APIChatEntities db = new APIChatEntities())
                {
                    //APIChat api = new APIChat();
                    //api.Nombre = sec.Nombre;
                    //api.Canal = sec.Canal;
                    //api.Mensaje = sec.Mensaje;
                    //db.APIChat.Add(api);
                    ////db.SaveChanges();
                    //string query = @"
                    //        update APIChat SET Mensaje = '" + sec.Mensaje + "', Fecha = '"+ tiempo +"' where Nombre = '" + sec.Nombre + "' and Canal = '"+sec.Canal + 
                    //        " ' select Nombre,Mensaje,Fecha from APIChat where Canal = '" + sec.Canal + "'";

                    string query = @"
                           insert into APIChat (Nombre, Mensaje, Canal, Fecha) VALUES ('" + login.Nombre + "', '" + login.Mensaje + "','" + login.Canal + "','" + tiempo + "' ) " +
                           "select Nombre,Mensaje,Fecha from APIChat where Canal = '" + login.Canal + "'";
                    using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["stringConexion"].ConnectionString))
                    using (var cmd = new SqlCommand(query, con))
                    using (var da = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.Text;
                        da.Fill(table);
                    }                   
                }
            }
            catch(Exception ex)
            {
                respuesta.mensaje = ex.Message;
            }
            return Request.CreateResponse(HttpStatusCode.OK, table);
        }                             
       
        [Route("api/usuario/Login")]
        [HttpGet]
        public dynamic IniciarSesion(Login login)
        {
            Respuesta respuesta = new Respuesta();

            respuesta.resultado = 0;

            if (login == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            using (APIChatEntities db = new APIChatEntities())
            {
                var list = db.APIChat.Where(d => d.Nombre == login.Nombre && d.Password == login.Password);

                if (list.Count() > 0)
                {
                    respuesta.resultado = 1;
                    respuesta.data = "exito";
                    APIChat chat = new APIChat();
                    APIChat.AgregarCanal(chat);
                    return Ok(respuesta.data);
                }
                else
                {
                    return Unauthorized();
                }
            }
        }
       
    }
}