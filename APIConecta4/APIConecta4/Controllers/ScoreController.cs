﻿using APIConecta4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace APIConecta4.Controllers
{
    public class ScoreController : ApiController
    {
        [Route("api/Score")]
        [HttpPost]
        public dynamic Retornar()
        {
            return "Esta es mi api";
        }
        [Route("api/Score/Crear")]
        [HttpPost]
        public dynamic Crear(Score score)
        {
            return Score.Agregar(score);
        }

    }
}