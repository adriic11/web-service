﻿using APIConecta4.Datos;
using APIConecta4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace APIConecta4.Controllers
{
    public class UsuarioController : ApiController
    {
        [Route("api/Usuario/Crear")]
        [HttpPost]
        public dynamic Crear(Usuario usuario)
        {
            return Usuario.Agregar(usuario);
        }

        [Route("api/Usuario/Login")]
        [HttpPost]
        public IHttpActionResult IniciarSesion(Login login)
        {

            Respuesta respuesta = new Respuesta();
            respuesta.resultado = 0;

            if (login == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            using (Connect_4Entities db = new Connect_4Entities())
            {
                var list = db.Usuario.Where(d => d.Nombre == login.Nombre && d.Password == login.Password);

                if (list.Count() > 0)
                {
                    respuesta.resultado = 1;
                    respuesta.data = TokenGenerator.GenerarToken(login.Nombre);

                    Usuario usuario = list.First();
                    usuario.Token = (string)respuesta.data;

                    Usuario.AgregarToken(usuario);
                    return Ok(respuesta.data);
                }
                else
                {
                    return Unauthorized();
                }

            }
        }

        [Route("api/Usuario/Autorizar")]
        [HttpPost]
        public dynamic Listar(Score score, HttpRequestMessage request)
        {
            Respuesta respuesta = new Respuesta();

            if(ValidarToken(request) == false)
            {
                respuesta.exito = false;
                respuesta.mensaje = "Token incorrecto";
                return respuesta;
            }
            else
            {
                return Score.Listar(score);
            }
        }

        public bool ValidarToken(HttpRequestMessage request)
        {
            string token = "";

            foreach (var item in request.Headers)
            {
                if (item.Key.Equals("Authorization"))
                {
                    token = item.Value.First();
                    break;
                }
            }

            using (Connect_4Entities bd = new Connect_4Entities())
            {
                if (bd.Usuario.Where(d => d.Token == token).Count() > 0)
                {
                    return true;
                }
            }

            return false;
        }

    }

}