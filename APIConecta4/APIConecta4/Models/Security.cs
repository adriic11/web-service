﻿using APIConecta4.Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIConecta4.Models
{
    public class Security
    {
        public string Token { get; set; }

        public static Respuesta Agregar (Security security)
        {
            List<Parametro> parametros = new List<Parametro>
            {
                new Parametro("@Token", security.Token)
            };

            return DBDatos.Ejecutar("Token_Agregar", parametros);
        }
    }
}